package com.crud.springcrud.services;

import com.crud.springcrud.entites.Users;
import com.crud.springcrud.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
@Service @RequiredArgsConstructor @Slf4j
public class UserServiceImpl implements UserServices, UserDetailsService {

   @Autowired
   private final UserRepository repo;

   @Autowired
   private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users user = repo.findByUsername(username);
        if(user != null){
            log.info("User found in the database : {}", username);
            log.info("Role User found in the database : {}", user.getRoleUser().name());
        }else {
            log.error("User not found in the database");
        }
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(user.getRoleUser().name()));
        return new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(),authorities);
    }

    @Override
    public Users Post(Users params) {
        Users usersExists = repo.findByUsername(params.getUsername());
        if (usersExists != null){
            throw  new RuntimeException(String.format("User with username '%s' already exists", params.getUsername()));
        }
        String encodePassword = bCryptPasswordEncoder.encode(params.getPassword());
        params.setPassword(encodePassword);
        repo.save(params);
        return params;
    }

    @Override
    public List<Users> Get() {
       return repo.findAll();
    }

    @Override
    public Users Get(Integer id) {
        return repo.findById(id).get();
    }

    @Override
    public Users Update(Users params, Integer id) {
       Optional<Users> userById = repo.findById(id);
       Users user = userById.get();
       user.setFirstName(params.getFirstName());
       user.setLastName(params.getLastName());
       user.setPassword(params.getPassword());
       user.setUsername(params.getUsername());
       user.setEmail(params.getEmail());

       return repo.save(user);
    }

    @Override
    public String Delete(Integer id) {
        repo.deleteById(id);
        return "User("+id+")"+"Has Been Deleted!";



    }


}
